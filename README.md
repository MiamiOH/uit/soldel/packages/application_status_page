# Application Status Page

## General Info
This package provides a standard way of incorporating a `/status` page in all laravel applications that are developed at [Miami University](http://www.miamioh.edu).  

---
> **NOTE**
> 
> The `/status` route and `status` route name are created by this package so should not be created by the application.

---
A route for `/status` is added to the application along with a controller to provide the needed information.  The package will process the handlers that are added to the Application Status object, and then display the status of your applications dependencies on the `/status` page.  The `/status` page can also be accessed as a webservice and the status of your application is viewable via JSON.

## Application Status Simple Layout ![Simple App Status Layout](./documentation/images/simpleLayout.png)

# Installation
## Add the Satis Repo
This package is managed via GitLab and published on our Satis server. If necessary, add the satis repo to your composer.json file:

```
"repositories": [
    {
        "type": "composer",
        "url": "https://satis.itapps.miamioh.edu/miamioh"
    }
]
```
## Composer Install 
To use the Application Status Page Package, all that is needed is to first run the following command from terminal. 
```
composer require "miamioh/application-status-page"
```

# Usage

To properly utilize the package minor configurations need to be done.
## Environment Settings
The application status information is by default configured to use the `@push` directive to add the information into the `content` section of a simpleLayout provided by the package.

### Using a Different Layout
If you would like to have the status information added to the content section of a different layout, then the layout would need to be updated using the `STATUS_THEME` environment variable.  

### Use a different Section
If your layout does not have a `content` section you may change the section that content is pushed to, using the `STATUS_SECTION` environment variable.   

### Sample ENV section that can be updated 
```
STATUS_THEME=simpleLayout
STATUS_SECTION=content
```
## Service Provider
To work properly a new ApplicationStatus Object needs to be created and bound so that it will always be returned to the package

## Create Variable  

The following code should be added to `/app/Providers/AppServiceProvider.php` in the `register()` method

First we need to instantiate a ApplicationStatus object.
```php
$applicationStatus = new ApplicationStatus();
```

## Create Handlers
A handler can be made to add any Status item that is necessary using code similar to the below code.

```php        
$applicationStatus->addHandler(function () {
    return new ConnectionDataPoint(
        WebConnectionDataPoint::class,
        new Label(['mars db']),
        new Measurements(),
        new Status('Success')
    );
});

$applicationStatus->addHandler(function () {
    return new ConnectionDataPoint(
        WebConnectionDataPoint::class,
        new Label(['Miami Homepage']),
        new Measurements(),
        new Status('Failure')
    );
});
```
## Specialized Handlers
### Database Handler Method
This package provides a standard method for adding a handler for a database connection, which can be invoked like so:

```php
$applicationStatus->addDatabaseHandler('label', 'database name');
```

This handler will do a basic connectivity check for the database connection:
```php
try {
    DB::connection($databaseName)->getPdo();
    $status = new Status('Success');
} catch (\Exception $e) {
    $status = new Status('Failure');
}
```

### Redis Handler Method
Similar to the database handler, the package provides a standard method for Redis status checks:
```php
$applicationStatus->addRedisHandler(['array', 'of', 'labels'], 'connection string')
```
**Note:** the label in the addRedisHandler method is an array.

### RestNg Handler Method
##### Authentication Check 
To validate that the user can successfully authenticate to the appropriate RestNg service.  Provide a MiamiOH\RESTng\Client\Endpoint and optional MiamiOH\RESTng\Client\TokenProvider to add a RestNg Authentication Check to the status page. 

```php
$endPointConfig = config('restng.endpoints.' . config('restng.default-endpoint'));
$endpoint = Endpoint(
    $endPointConfig['url'],
    $endPointConfig['username'],
    $endPointConfig['password']
);

$applicationStatus->addRestNgAuthenticationCheck($endpoint, $tokenProvider);
```

### Configuration Service (CAM 3.x) Handler Method
##### Authentication Check 
Validate that the user is authorized to access the appropriate CAM listing. Provide a MiamiOH\RESTng\Client\Agent with a valid MiamiOH\RESTng\Client\Endpoint, the CAM application name, and an existing category. 

```php
$applicationStatus->addConfigurationServiceCheck(
    new Agent($endpoint),
    config('config-mgr.default-application'),
    config('config-mgr.default-category')
);
```

### Bind ApplicationStatus to App
The ApplicationStatus instance needs to be bound to the application, so it can be provided whenever an ApplicationStatus object is needed.  When all pieces are combined it can look like the following.  
```php
$this->app->bind(ApplicationStatus::class, function ($app) {
    $applicationStatus = new ApplicationStatus();
    
    $applicationStatus->addHandler(function () {
        return new ConnectionDataPoint(
            WebConnectionDataPoint::class,
            new Label(['mars db']),
            new Measurements(),
            new Status('Success')
        );
    });

    $applicationStatus->addHandler(function () {
        return new ConnectionDataPoint(
            WebConnectionDataPoint::class,
            new Label(['Miami Homepage']),
            new Measurements(),
            new Status('Failure')
        );
    });
    return $applicationStatus; 
});
```

#### Automatic Discovery of Handlers
This package also provides a way to automatically discover and add handlers. If you implement the `\MiamiOH\ApplicationStatus\Interfaces\StatusCheck.php` interface with one or more classes in your project's code base, you can call a method to automatically add handlers for those classes like so:
```php
$this->app->bind(ApplicationStatus::class, function () {
    $applicationStatus = new ApplicationStatus();
    $classes = [QuickLinksStatusCheck::class];
    $applicationStatus->discoverAndAddHandlers($classes);

    return $applicationStatus;
});
```
You do this instead of adding the custom handlers in the `AppServiceProvider.php` file. And you need to make sure the $classes array contains the classes you want the application to parse. (We do this to avoid looping through a lot of extra files.)
You can review the discoverAndAddHandlers in the `ApplicationStatus.php` class of this repository.

## Contributing to this package
See the [Contributing.md](CONTRIBUTING.md) file for information on how to contribute to this package.
