<?php

namespace MiamiOH\ApplicationStatus;

use Exception;

/**
 * Class Status
 * -- This is the Status Value Object used in creation of a DataPoint
 *
 * @package MiamiOH\Status
 */
class Status
{
    /**
     * @var string
     */
    private $status;

    /**
     * Status constructor.
     * @param string $status
     * @throws Exception
     */
    public function __construct(string $status)
    {
        $this->validateStatusValues($status);
    }

    private function validateStatusValues(string $status)
    {
        if ((strtoupper($status) == 'SUCCESS') || (strtoupper($status) == 'FAILURE')) {
            $this->status = $status;
        } else {
            throw new Exception($status . ' is not a valid status value');
        }
    }

    public function __toString(): string
    {
        return $this->status;
    }
}
