<?php

namespace MiamiOH\ApplicationStatus\DataPoint;

use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use MiamiOH\ApplicationStatus\Label;
use MiamiOH\ApplicationStatus\Measure;
use MiamiOH\ApplicationStatus\Measurements;
use MiamiOH\ApplicationStatus\Status;

class HttpConnectionDataPoint extends ConnectionDataPoint
{
    public static function httpConnection(Client $client, string $url, string $connectionName, array $expectedStatusCodes = [200])
    {
        //string $dataPointType, Label $label, Measurements $measures, Status $status
        $measurements = new Measurements();
        $startTime = new Measure('startTime', Carbon::now()->timestamp);
        $measurements->add($startTime);
        try {
            $response = $client->get($url);
            $status = in_array($response->getStatusCode(), $expectedStatusCodes) ? new Status('Success') : new Status('Failure');
        } catch (Exception $exception) {
            $statusCode = new Measure('statusCode', $exception->getCode());
            $measurements->add($statusCode);
            $status = new Status('Failure');
        }
        $stopTime = new Measure('stopTime', Carbon::now()->timestamp);
        $measurements->add($stopTime);
        $label = new Label([$connectionName]);
        return new HttpConnectionDataPoint(self::class, $label, $measurements, $status);
    }
}
