<?php

namespace MiamiOH\ApplicationStatus\DataPoint;

use Illuminate\Support\Collection;
use MiamiOH\ApplicationStatus\Exceptions\InvalidArgumentException;

class DataPointCollection extends Collection
{
    /**
     * @param mixed $item
     * @return Collection
     * @throws InvalidArgumentException
     */
    public function add($item): Collection
    {
        if (!$item instanceof DataPoint) {
            throw new InvalidArgumentException('Must be of class ' . DataPoint::class);
        }
        $this->addDataPoint($item);

        return $this;
    }

    /**
     * @param DataPoint $item
     */
    public function addDataPoint(DataPoint $item)
    {
        parent::add($item);
    }
}
