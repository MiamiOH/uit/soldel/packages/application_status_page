<?php

namespace MiamiOH\ApplicationStatus\DataPoint;

use Carbon\Carbon;
use Exception;
use JsonSerializable;
use MiamiOH\ApplicationStatus\Measure;

/**
 * Class ConnectionDataPoint
 *
 * Design Ideas
 * Would contain specific information about data points for connections
 *  --  Database Connections
 *  --  WebService connections
 * Should make specific ConnectionDataPoint Classes for different types of connections
 *  --  DataBaseConnectionDataPoint
 *  --  WebServiceConnectionDataPoint
 *
 * @package MiamiOH`\Status
 */
class ConnectionDataPoint extends DataPoint implements JsonSerializable
{
    public function startTimer()
    {
        $this->addMeasurement('startTime', Carbon::now()->timestamp);
    }

    public function addMeasurement(string $measurementName, float $value)
    {
        $this->measurements->add(new Measure($measurementName, $value));
    }

    public function stopTimer()
    {
        $this->addMeasurement('stopTime', Carbon::now()->timestamp);
        $this->calculateDuration();
    }

    protected function calculateDuration()
    {
        try {
            $startTime = $this->measures()->getMeasure('startTime')->value();
            $stopTime = $this->measures()->getMeasure('stopTime')->value();
            $duration = Carbon::createFromTimestamp($stopTime)->diffInMilliseconds(Carbon::createFromTimestamp($startTime));
            $this->addMeasurement('duration', $duration);
        } catch (Exception $e) {
        }
    }

    public function getData(): array
    {
        return [
            'label' => $this->label(),
            'status' => $this->status()
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->getData();
    }
}
