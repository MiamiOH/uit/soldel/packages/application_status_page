<?php

namespace MiamiOH\ApplicationStatus\DataPoint;

use MiamiOH\ApplicationStatus\Label;
use MiamiOH\ApplicationStatus\Measurements;
use MiamiOH\ApplicationStatus\Status;

abstract class DataPoint
{
    /**
     * @var Measurements
     */
    protected $measurements;
    /**
     * @var string
     */
    private $dataPointType;
    /**
     * @var Label
     */
    private $label;
    /**
     * @var Status
     */
    private $status;

    /**
     * DataPoint constructor.
     * @param string $dataPointType
     * @param Label $label
     * @param Measurements $measures
     * @param Status $status
     */
    public function __construct(string $dataPointType, Label $label, Measurements $measures, Status $status)
    {
        $this->dataPointType = $dataPointType;
        $this->label = $label;
        $this->measurements = $measures;
        $this->status = $status;
    }

    public function measures(): Measurements
    {
        return $this->measurements;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function label(): string
    {
        return $this->label;
    }

    public function dataPointType(): string
    {
        return $this->dataPointType;
    }
}
