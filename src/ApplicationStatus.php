<?php

namespace MiamiOH\ApplicationStatus;

use Carbon\Carbon;
use Exception;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use JsonException;
use MiamiOH\ApplicationStatus\DataPoint\ConnectionDataPoint;
use MiamiOH\ApplicationStatus\DataPoint\DataPointCollection;
use MiamiOH\ApplicationStatus\DataPoint\RestNgDataPoint;
use MiamiOH\RESTng\Client\Agent;
use MiamiOH\RESTng\Client\Endpoint;
use MiamiOH\RESTng\Client\RestNgClientException;
use MiamiOH\RESTng\Client\TokenProvider;
use ReflectionClass;
use ReflectionException;
use Throwable;

class ApplicationStatus
{
    /**
     * @var Collection
     */
    private $collectors;
    private $STATUS_CHECK_CLASS = 'MiamiOH\ApplicationStatus\Interfaces\StatusCheck';

    /**
     * ApplicationStatus constructor.
     */
    public function __construct()
    {
        $this->collectors = new Collection();
    }

    /**
     * Method that iterates  through all the callable code that has been added and returns a collection of DataPoints
     * @return DataPointCollection
     */
    public function getDataPoints(): DataPointCollection
    {
        return $this->collectors->reduce(function (DataPointCollection $collection, callable $handler) {
            try {
                $dataPoint = $handler();
            } catch (Throwable $e) {
                $label = new Label(['Unknown DataPoint', ' Error getting DataPoint'], ':');
                $measurements = new Measurements();
                $status = new Status('Failure');
                $dataPoint = new ConnectionDataPoint(ConnectionDataPoint::class, $label, $measurements, $status);
            }
            $collection->add($dataPoint);
            return $collection;
        }, new DataPointCollection());
    }

    /**
     * @param string $label
     * @param string $databaseName
     */
    public function addDatabaseHandler(string $label, string $databaseName): void
    {
        $this->addHandler(function () use ($label, $databaseName) {
            return $this->getDatabaseConnectionDataPoint($label, $databaseName);
        });
    }

    /**
     * @param callable $callable
     */
    public function addHandler(callable $callable): void
    {
        $this->collectors->add($callable);
    }

    /**
     * @param string $label
     * @param string $databaseName
     * @return ConnectionDataPoint
     * @throws Exceptions\InvalidArgumentException
     */
    public static function getDatabaseConnectionDataPoint(string $label, string $databaseName): ConnectionDataPoint
    {
        $label = new Label([$label]);
        $measurements = new Measurements();
        $measurements->add(new Measure('start time', Carbon::now()->timestamp));

        try {
            DB::connection($databaseName);
            $status = new Status('Success');
        } catch (Exception $e) {
            $status = new Status('Failure');
        }
        $measurements->add(new Measure('stop time', Carbon::now()->timestamp));
        return new ConnectionDataPoint(ConnectionDataPoint::class, $label, $measurements, $status);
    }

    /**
     * Method that will call the StatusCheck method on all provided classes
     *
     * If no classes are provided it will search for all classes that have the StatusCheck Interface implemented
     *   and will then call the statusCheck method in those classes.
     *
     * @throws ReflectionException
     */
    public function discoverAndAddHandlers($in_classes = null): void
    {
        $classes = $in_classes ?? get_declared_classes();

        foreach ($classes as $className) {
            if (in_array($this->STATUS_CHECK_CLASS, class_implements($className))) {
                $testClass = new ReflectionClass($className);
                if ($testClass->isInstantiable()) {
                    {
                        $this->addHandler(function () use ($className) {
                            $class = app($className);
                            return $class->StatusCheck();
                        });
                    }
                }
            }
        }
    }

    public function addRedisHandler(array $connectionLabel, string $connection = null): void
    {
        $this->addHandler(function () use ($connectionLabel, $connection) {
            $label = new Label($connectionLabel);
            $measurements = new Measurements();
            $measurements->add(new Measure('start time', Carbon::now()->timestamp));

            try {
                Redis::connection($connection);
                $status = new Status('Success');
            } catch (Exception $e) {
                $status = new Status('Failure');
            }

            $measurements->add(new Measure('stop time', Carbon::now()->timestamp));
            return new ConnectionDataPoint(ConnectionDataPoint::class, $label, $measurements, $status);
        });
    }

    public function addRestNgAuthenticationCheck(Endpoint $endpoint, TokenProvider $tokenProvider = null)
    {
        $tokenProvider = $tokenProvider ?? new TokenProvider();
        $this->addHandler(function () use ($endpoint, $tokenProvider) {
            $connectionLabel[] = 'RestNg';
            $connectionLabel[] = 'Authentication';
            $connectionLabel[] = $endpoint->url();
            $connectionLabel[] = $endpoint->username();

            $label = new Label($connectionLabel, ':');
            $measurements = new Measurements();
            $measurements->add(new Measure('start time', Carbon::now()->timestamp));
            try {
                $token = $tokenProvider->getToken($endpoint);
                $status = new Status('Success');
            } catch (RestNgClientException | JsonException $exception) {
                $status = new Status('Failure');
            }
            $measurements->add(new Measure('stop time', Carbon::now()->timestamp));
            return new RestNgDataPoint(RestNgDataPoint::class, $label, $measurements, $status);
        });
    }

    // AuthN check for CAM 3.x
    public function addConfigurationServiceCheck(Agent $agent, string $application, string $category)
    {
        $this->addHandler(function () use ($agent, $application, $category): RestNgDataPoint {
            $connectionLabel[] = 'CAM';
            $connectionLabel[] = 'ConfigurationService';
            $connectionLabel[] = 'Authorization';

            $label = new Label($connectionLabel, ':');
            $measurements = new Measurements();
            $measurements->add(new Measure('start time', Carbon::now()->timestamp));
            try {
                $response = $agent->run(
                    new Request('GET', sprintf('/api/config/v1/%s?format=complete&category=%s', $application, $category))
                );

                if ($response->status() !== 200) {
                    throw new RestNgClientException('Unauthorized');
                }

                $status = new Status('Success');
            } catch (RestNgClientException | JsonException $exception) {
                $status = new Status('Failure');
            }
            $measurements->add(new Measure('stop time', Carbon::now()->timestamp));
            return new RestNgDataPoint(RestNgDataPoint::class, $label, $measurements, $status);
        });
    }
}
