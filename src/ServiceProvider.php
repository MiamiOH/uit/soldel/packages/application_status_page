<?php

namespace MiamiOH\ApplicationStatus;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use MiamiOH\ApplicationStatus\Http\Controllers\ApplicationStatusController;

class ServiceProvider extends BaseServiceProvider
{
    private $configPath = __DIR__ . '/../config/miamioh-appStatus.php';

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'ApplicationStatus');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->publishes([
            $this->configPath => config_path('miamioh-appStatus.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function register()
    {
        $this->app->make(ApplicationStatusController::class);
        $this->mergeConfigFrom($this->configPath, 'miamioh-appStatus');
        $this->app->instance(ApplicationStatus::class, new ApplicationStatus());
    }
}
