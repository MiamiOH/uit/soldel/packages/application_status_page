<?php

namespace MiamiOH\ApplicationStatus\Interfaces;

use MiamiOH\ApplicationStatus\DataPoint\ConnectionDataPoint;

interface StatusCheck
{
    public function StatusCheck(): ConnectionDataPoint;
}
