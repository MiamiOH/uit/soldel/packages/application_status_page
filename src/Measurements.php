<?php

namespace MiamiOH\ApplicationStatus;

// Collection of Measure

use Exception;
use Illuminate\Support\Collection;
use MiamiOH\ApplicationStatus\Exceptions\InvalidArgumentException;

class Measurements extends Collection
{
    /**
     * @param mixed $item
     * @return $this|Measurements
     * @throws InvalidArgumentException
     */
    public function add($item)
    {
        if (!$item instanceof Measure) {
            throw new InvalidArgumentException('Must be of class ' . Measure::class);
        }
        parent::add($item);

        return $this;
    }

    /**
     * @param string $measureName
     * @return Measure
     * @throws Exception
     */
    public function getMeasure(string $measureName): Measure
    {
        foreach ($this as $value) {
            if ($value->name() == $measureName) {
                return $value;
            }
        }
        throw new Exception($measureName . ' is not in list of measurements');
    }
}
