<?php

namespace MiamiOH\ApplicationStatus;

/**
 * Class Label
 *
 * Design Ideas
 *  -- This Class will handle the labeling of the DataPoints
 *  -- Current thought is that the labels will just be a series of strings
 *  -- built in a increasingly specific order ['database', 'DBName', 'schema', 'User']
 *
 * @package MiamiOH\Status
 */
class Label
{
    public const DELIMITER = '.';
    /**
     * @var array
     */
    private $labelValues;
    /**
     * @var string
     */
    private $delimiter;

    /**
     * Label constructor.
     * @param array $labelValues
     */
    public function __construct(array $labelValues, string $delimiter = self::DELIMITER)
    {
        $this->labelValues = $labelValues;
        $this->delimiter = $delimiter;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode($this->delimiter, $this->labelValues);
    }
}
