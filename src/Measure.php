<?php

namespace MiamiOH\ApplicationStatus;

/**
 * Class Measure
 *
 * Design Ideas
 * Could start as Key/Value Pairs of information
 * Could add something to handle certain types of measures
 *  -- default duration time format (default MilliSeconds) Ability to set differently.
 * May want to consider adding a naming comvention for the measure
 *
 * @package MiamiOH\Status
 */
class Measure
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var float
     */
    private $value;

    /**
     * Measure constructor.
     * @param string $name
     * @param float $value
     */
    public function __construct(string $name, float $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    public function name()
    {
        return $this->name;
    }

    public function value()
    {
        return $this->value;
    }
}
