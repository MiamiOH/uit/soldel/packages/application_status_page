<?php

namespace MiamiOH\ApplicationStatus\Http\Controllers;

use MiamiOH\ApplicationStatus\ApplicationStatus;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ApplicationStatusController extends Controller
{
    /**
     * @var ApplicationStatus
     */
    private $applicationStatus;

    /**
     * ApplicationStatusController constructor.
     */
    public function __construct(ApplicationStatus $applicationStatus)
    {
        $this->applicationStatus = $applicationStatus;
    }

    public function getStatus(Request $request)
    {
        $dataPointCollection = $this->applicationStatus->getDataPoints();

        $acceptHeader = $request->header('accept');
        if (strpos($acceptHeader, 'application/json') !== false) {
            return response()->json($dataPointCollection);
        } else {
            return view('ApplicationStatus::appStatus', compact('dataPointCollection'));
        }
    }
}
