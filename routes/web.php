<?php

use Illuminate\Support\Facades\Route;
use MiamiOH\ApplicationStatus\Http\Controllers\ApplicationStatusController;

Route::get('/status', [ApplicationStatusController::class, 'getStatus'])->name('status');
