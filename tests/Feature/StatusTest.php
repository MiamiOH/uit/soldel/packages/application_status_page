<?php

namespace Tests\MiamiOH\ApplicationStatus\Feature;

use Illuminate\Http\Response;
use MiamiOH\ApplicationStatus\ApplicationStatus;
use MiamiOH\ApplicationStatus\DataPoint\ConnectionDataPoint;
use MiamiOH\ApplicationStatus\DataPoint\HttpConnectionDataPoint;
use MiamiOH\ApplicationStatus\Label;
use MiamiOH\ApplicationStatus\Measurements;
use MiamiOH\ApplicationStatus\Status;
use Tests\MiamiOH\ApplicationStatus\TestCase;

class StatusTest extends TestCase
{
    /**
     * @var ApplicationStatus
     */
    public function testCanGetBlankStatusPage()
    {
        $response = $this->get('/status');
        $response->assertStatus(200);
        $response->assertSee('Application Status');
    }

    public function testCanGetEmptyJsonStatusPage()
    {
        $this->json('get', '/status', ['accept' => 'application/json'])
            ->assertStatus(200);
    }

    public function testCanDisplayConnectionDataPointOnPage()
    {
        app()->make(ApplicationStatus::class)->addHandler(function () {
            return new ConnectionDataPoint(
                'WebConnectionDataPoint::class',
                new Label(['WebConnection']),
                new Measurements(),
                new Status('Failure')
            );
        });

        $response = $this->get('/status');
        $response->assertStatus(200);
        $response->assertSee('Failure');
    }

    public function testCanDisplayDataPointInJson()
    {
        $this->withoutExceptionHandling();

        app()->make(ApplicationStatus::class)->addHandler(function () {
            return new ConnectionDataPoint(
                'WebConnectionDataPoint::class',
                new Label(['WebConnection']),
                new Measurements(),
                new Status('Failure')
            );
        });

        $response = $this->json('get', '/status', ['accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [['label', 'status']]
        );
        $response->assertExactJson(
            [
                [
                    'label' => 'WebConnection',
                    'status' => 'Failure'
                ]
            ]
        );
    }

    public function testCanDisplayWebConnectionDataPointOnPage()
    {
        app()->make(ApplicationStatus::class)->addHandler(function () {
            return new HttpConnectionDataPoint(
                'WebConnectionDataPoint::class',
                new Label(['WebConnection']),
                new Measurements(),
                new Status('Failure')
            );
        });

        $response = $this->get('/status');
        $response->assertOK();
        $response->assertSee('Failure');
    }

    public function testCanDisplayErrorWhenCallableThrowsError()
    {
        app()->make(ApplicationStatus::class)->addHandler(function () {
            trigger_error('Error Thrown');
        });
        $response = $this->get('/status');
        $response->assertOK();
        $response->assertSee('Failure');
        $response->assertSeeText('Unknown DataPoint');
    }
    public function testCanDisplayErrorWhenCallableThrowsException()
    {
        app()->make(ApplicationStatus::class)->addHandler(function () {
            throw New \Exception('Exception Thrown');
        });
       $response = $this->get('/status');
        $response->assertOK();
        $response->assertSee('Failure');
        $response->assertSeeText('Unknown DataPoint');
    }
    public function testReturnDoesNotFailWhenCallableThrowsException()
    {
        app()->make(ApplicationStatus::class)->addHandler(function () {
            return new ConnectionDataPoint(
                'WebConnectionDataPoint::class',
                new Label(['WebConnection1']),
                new Measurements(),
                new Status('Success')
            );
        });
        app()->make(ApplicationStatus::class)->addHandler(function () {
            throw New \Exception('Exception Thrown');
        });
        app()->make(ApplicationStatus::class)->addHandler(function () {
            return new ConnectionDataPoint(
                'WebConnectionDataPoint::class',
                new Label(['WebConnection3']),
                new Measurements(),
                new Status('Success')
            );
        });

        $response = $this->get('/status');
        $response->assertOK();
        $response->assertSee('Failure');
        $response->assertSeeText('WebConnection1');
        $response->assertSeeText('Unknown DataPoint');
        $response->assertSeeText('WebConnection3');
    }


    protected function setUp(): void
    {
        parent::setUp();
    }
}
