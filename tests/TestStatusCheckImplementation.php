<?php


namespace Tests\MiamiOH\ApplicationStatus;


use MiamiOH\ApplicationStatus\DataPoint\ConnectionDataPoint;
use MiamiOH\ApplicationStatus\Interfaces\StatusCheck;
use MiamiOH\ApplicationStatus\Label;
use MiamiOH\ApplicationStatus\Measurements;
use MiamiOH\ApplicationStatus\Status;

class TestStatusCheckImplementation implements StatusCheck
{
    // This class exists for testing and does implement status check interface

    public function StatusCheck(): ConnectionDataPoint
    {
        $label = new Label(['test label']);
        $measurements = new Measurements();
        return new ConnectionDataPoint(ConnectionDataPoint::class, $label, $measurements, new Status('Success'));
    }
}