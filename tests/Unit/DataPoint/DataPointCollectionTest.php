<?php

namespace Tests\MiamiOH\ApplicationStatus\Unit\DataPoint;

use MiamiOH\ApplicationStatus\DataPoint\ConnectionDataPoint;
use MiamiOH\ApplicationStatus\DataPoint\DataPointCollection;
use MiamiOH\ApplicationStatus\Exceptions\InvalidArgumentException;
use MiamiOH\ApplicationStatus\Label;
use MiamiOH\ApplicationStatus\Measurements;
use MiamiOH\ApplicationStatus\Status;
use Tests\MiamiOH\ApplicationStatus\TestCase;

class DataPointCollectionTest extends TestCase
{
    public function testCanRenderJson()
    {
        $collection = new DataPointCollection();
        $collection->addDataPoint($this->createConnectionDataPoint());
        $json = $collection->jsonSerialize();
        $this->assertJson("{\"label\":\"test\",\"status\":\"Success\"}", json_encode($json));
    }

    public function testCanAddOneDataPointToCollection()
    {
        $collection = new DataPointCollection();
        $collection->addDataPoint($this->createConnectionDataPoint());
        $this->assertCount(1, $collection);
    }

    public function testCanAddTwoDataPointToCollection()
    {
        $collection = new DataPointCollection();
        $collection->addDataPoint($this->createConnectionDataPoint());
        $collection->addDataPoint($this->createConnectionDataPoint());
        $this->assertCount(2, $collection);
    }

    protected function createConnectionDataPoint()
    {
        $dataPoint = new ConnectionDataPoint(
            'WebConnectionDataPoint::class',
            new Label(['Test']),
            new Measurements(),
            new Status('Success')
        );
        return $dataPoint;
    }

    public function testCanThrowExceptionWhenAddingNoneDataPoint()
    {
        $collection = new DataPointCollection();
        $this->expectException(InvalidArgumentException::class);
        $collection->add('xyz');
    }

    protected function setUp(): void
    {
        parent::setUp();
    }
}
