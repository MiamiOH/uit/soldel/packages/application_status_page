<?php

namespace Tests\MiamiOH\ApplicationStatus\Unit\DataPoint;

use MiamiOH\ApplicationStatus\DataPoint\ConnectionDataPoint;
use MiamiOH\ApplicationStatus\DataPoint\HttpConnectionDataPoint;
use MiamiOH\ApplicationStatus\Label;
use MiamiOH\ApplicationStatus\Measurements;
use MiamiOH\ApplicationStatus\Status;
use Tests\MiamiOH\ApplicationStatus\Unit\ClientTestCase;

class HttpConnectionDataPointTest extends ClientTestCase
{
    private $label;
    /**
     * @var Measurements
     */
    private $measures;
    /**
     * @var Status
     */
    private $dpStatus;

    public function setUp(): void
    {
        $this->label = new Label(['Test']);
        $this->measures = new Measurements();
        $this->dpStatus = new Status('Success');
    }

    public function testCanConnectToWebService()
    {
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse(['data' => '123'])
        ]);
        $dataPoint = HttpConnectionDataPoint::httpConnection($client, 'test', 'TestWebService');
        $this->assertCount(2, $dataPoint->measures());
//        $this->assertEquals(200, $dataPoint->measures()->getMeasure('TestWebService')->value());
        $this->assertInstanceOf(ConnectionDataPoint::class, $dataPoint);
    }

    public function testConnectToWebServiceWhenDefaultStatusUsed(): void
    {
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse(['data' => '123'], )
        ]);

        $dataPoint = HttpConnectionDataPoint::httpConnection($client, 'test', 'TestWebService');
        $this->assertEquals('Success', $dataPoint->status());
    }

    /**
    * @dataProvider httpTestValues
    **/
    public function testConnectToWebServiceWithVariousStatusCodes(int $httpStatus, array $statusArray, string $expected): void
    {
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse(['data' => '123'], $httpStatus)
        ]);

        $dataPoint = HttpConnectionDataPoint::httpConnection($client, 'test', 'TestWebService', $statusArray);
        $this->assertEquals($expected, $dataPoint->status());
    }

    public static function httpTestValues():array
    {
        return [
            'Only 200' => [200, [200], 'Success'],
            '200 - Multiple possible Values' => [200, [200], 'Success'],
            '202 - Multiple possible Values' => [202, [200,202], 'Success'],
            '302 - Multiple possible Values' => [302, [200,202], 'Failure'],
            '404 - Multiple possible Values' => [404, [200,202], 'Failure'],
            '500 - Multiple possible Values' => [500, [200,202], 'Failure'],
        ];
    }
}
