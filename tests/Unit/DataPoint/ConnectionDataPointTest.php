<?php

namespace Tests\MiamiOH\ApplicationStatus\Unit\DataPoint;

use Carbon\Carbon;
use MiamiOH\ApplicationStatus\DataPoint\ConnectionDataPoint;
use MiamiOH\ApplicationStatus\Label;
use MiamiOH\ApplicationStatus\Measurements;
use MiamiOH\ApplicationStatus\Status;
use Tests\MiamiOH\ApplicationStatus\TestCase;

class ConnectionDataPointTest extends TestCase
{
    /**
     * @var Label
     */
    protected $label;
    /**
     * @var Measurements
     */
    protected $measures;
    /**
     * @var Carbon
     */
    protected $startTime;
    /**
     * @var Carbon
     */
    protected $stopTime;
    /**
     * @var Status
     */
    protected $dpStatus;
    /**
     * @var int
     */
    protected $duration;

    public function setUp(): void
    {
        $this->label = new Label(['Test']);
        $this->measures = new Measurements();
        $this->dpStatus = new Status('Success');
        $this->startTime = Carbon::create(2001, 5, 21, 12, 5, 10);
        $this->stopTime = Carbon::create(2001, 5, 21, 12, 5, 15);
        $this->duration = $this->stopTime->diffInMilliseconds($this->startTime);
    }

    public function testCanCreateDataPoint()
    {
        $dataPoint = new ConnectionDataPoint('TestDataPoint', $this->label, $this->measures, $this->dpStatus);
        $this->assertInstanceOf(ConnectionDataPoint::class, $dataPoint);
        $this->assertCount(0, $dataPoint->measures());
    }

    public function testCanGetDataPointType()
    {
        $dataPoint = new ConnectionDataPoint('TestDataPoint', $this->label, $this->measures, $this->dpStatus);
        $this->assertInstanceOf(ConnectionDataPoint::class, $dataPoint);
        $this->assertCount(0, $dataPoint->measures());
        $this->assertEquals('TestDataPoint',$dataPoint->dataPointType());
    }
    public function testCanAddStartTimeToDataPoint()
    {
        Carbon::setTestNow($this->startTime);
        $dataPoint = new ConnectionDataPoint('TestDataPoint', $this->label, $this->measures, $this->dpStatus);
        $dataPoint->startTimer();
        $this->assertCount(1, $dataPoint->measures());
        $this->assertInstanceOf(ConnectionDataPoint::class, $dataPoint);
        $this->assertEquals($this->startTime->timestamp, $dataPoint->measures()->getMeasure('startTime')->value());
    }

    public function testCanAddStopTimeToDataPoint()
    {
        Carbon::setTestNow($this->stopTime);
        $dataPoint = new ConnectionDataPoint('TestDataPoint', $this->label, $this->measures, $this->dpStatus);
        $dataPoint->stopTimer();
        $this->assertCount(1, $dataPoint->measures());
        $this->assertEquals($this->stopTime->timestamp, $dataPoint->measures()->getMeasure('stopTime')->value());
        $this->assertInstanceOf(ConnectionDataPoint::class, $dataPoint);
    }

    public function testCanCalculateDurationToDataPoint()
    {
        $dataPoint = new ConnectionDataPoint('TestDataPoint', $this->label, $this->measures, $this->dpStatus);
        Carbon::setTestNow($this->startTime);
        $dataPoint->startTimer();
        Carbon::setTestNow($this->stopTime);
        $dataPoint->stopTimer();
        $this->assertCount(3, $dataPoint->measures());
        $this->assertEquals($this->stopTime->timestamp, $dataPoint->measures()->getMeasure('stopTime')->value());
        $this->assertEquals($this->duration, $dataPoint->measures()->getMeasure('duration')->value());
        $this->assertInstanceOf(ConnectionDataPoint::class, $dataPoint);
    }

    public function testDurationNotCreatedIfStartTimeNotSet()
    {
        $dataPoint = new ConnectionDataPoint('TestDataPoint', $this->label, $this->measures, $this->dpStatus);
        Carbon::setTestNow($this->stopTime);
        $dataPoint->stopTimer();
        $this->assertCount(1, $dataPoint->measures());
        $this->assertInstanceOf(ConnectionDataPoint::class, $dataPoint);
    }
    public function testCanGetDataForDataPoint()
    {
        $dataPoint = new ConnectionDataPoint('TestDataPoint', $this->label, $this->measures, $this->dpStatus);

        $data = $dataPoint->getData();
        $this->assertEquals(['label'=>'Test','status'=>'Success'], $data);
    }

    public function testCanJsonRenderASingleWebDataPoint()
    {
        $dataPoint = new ConnectionDataPoint('TestDataPoint', $this->label, $this->measures, $this->dpStatus);
        $renderedJson = json_encode($dataPoint->jsonSerialize());
        $this->assertJson($renderedJson);
        $this->assertJsonStringEqualsJsonString(
            '{"label":"Test","status":"Success"}',
            $renderedJson
        );
    }
    public function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }
}
