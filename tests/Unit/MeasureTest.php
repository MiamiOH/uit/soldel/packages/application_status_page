<?php

namespace Tests\MiamiOH\ApplicationStatus\Unit;

use MiamiOH\ApplicationStatus\Measure;
use Tests\MiamiOH\ApplicationStatus\TestCase;

class MeasureTest extends TestCase
{
    /**
     * @var Measure
     */
    private $measure;
    /**
     * @var float
     */
    private $value;
    /**
     * @var string
     */
    private $measureName;

    public function setUp(): void
    {
        $this->measureName = 'Duration';
        $this->value = 111.789;
        $this->measure = new Measure($this->measureName, $this->value);
    }

    public function testCanCreateAMeasure()
    {
        $this->assertInstanceOf(measure::class, $this->measure);
    }

    public function testCanGetName()
    {
        $this->assertEquals($this->measureName, $this->measure->name());
    }

    public function testCanGetValue()
    {
        $this->assertEquals($this->value, $this->measure->value());
    }
}
