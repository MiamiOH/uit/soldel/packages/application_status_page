<?php

namespace Tests\MiamiOH\ApplicationStatus\Unit;

use MiamiOH\ApplicationStatus\Label;
use Tests\MiamiOH\ApplicationStatus\TestCase;

class LabelTest extends TestCase
{
    /**
     * @var Label
     */
    private $label;

    public function setUp(): void
    {
        $this->label = new Label(['database', 'DBName', 'schema', 'User']);
    }

    public function testCanCreateLabel()
    {
        $this->assertInstanceOf(Label::class, $this->label);
    }

    public function testCanRenderLabelWithDefaultDelimiter()
    {
        $this->assertEquals('database.DBName.schema.User', $this->label);
    }

    public function testCanRenderLabelWithDashAsDelimiter()
    {
        $label = new Label(['database', 'DBName', 'schema', 'User'], '-');
        $this->assertEquals('database-DBName-schema-User', $label);
    }
}
