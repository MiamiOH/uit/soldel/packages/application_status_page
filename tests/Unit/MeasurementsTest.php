<?php

namespace Tests\MiamiOH\ApplicationStatus\Unit;

use MiamiOH\ApplicationStatus\Exceptions\InvalidArgumentException;
use MiamiOH\ApplicationStatus\Measure;
use MiamiOH\ApplicationStatus\Measurements;
use Tests\MiamiOH\ApplicationStatus\TestCase;

class MeasurementsTest extends TestCase
{
    /**
     * @var Measurements
     */
    private $measurement;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $measure;

    public function setUp(): void
    {
        $this->measurement = new Measurements();
        $this->measure = $this->createMock(Measure::class);
    }

    public function testMeasurementsContainNoMeasuresWhenCreated()
    {
        $this->assertCount(0, $this->measurement);
    }

    public function testCanAddMeasure()
    {
        $this->measurement->add($this->measure);
        $this->assertCount(1, $this->measurement);
    }

    public function testWillThrowExceptionWhenNotAddingMeasure()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->measurement->add('123');
    }

    public function testCanGetAMeasure()
    {
        $measure = new Measure('Test', 123456);
        $measurement = new Measurements();
        $measurement->add($measure);
        $measure = new Measure('Test2', 987654);
        $measurement->add($measure);
        $this->assertCount(2, $measurement);
        $this->assertEquals(123456, $measurement->getMeasure('Test')->value());
    }
}
