<?php

namespace Tests\MiamiOH\ApplicationStatus\Unit;

use Exception;
use MiamiOH\ApplicationStatus\Status;
use Tests\MiamiOH\ApplicationStatus\TestCase;

class StatusTest extends TestCase
{
    public function testCanCreateStatusObject()
    {
        $status = new Status('Success');
        $this->assertInstanceOf(Status::class, $status);
    }

    public function testWillReturnExceptionsWhenInvalidStatusUsed()
    {
        $this->expectException(Exception::class);
        new Status('Kinda Worked');
    }

    public function testCanReturnStatusAsString()
    {
        $status = new Status('Success');
        $this->assertEquals('Success', $status->__toString());
    }
}
