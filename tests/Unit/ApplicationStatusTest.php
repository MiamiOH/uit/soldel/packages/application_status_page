<?php

namespace Tests\MiamiOH\ApplicationStatus\Unit;

use Carbon\Carbon;
use function PHPUnit\Framework\throwException;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use MiamiOH\ApplicationStatus\ApplicationStatus;
use MiamiOH\ApplicationStatus\DataPoint\DataPointCollection;
use MiamiOH\ApplicationStatus\DataPoint\HttpConnectionDataPoint;
use MiamiOH\ApplicationStatus\DataPoint\RestNgDataPoint;
use MiamiOH\ApplicationStatus\Exceptions\InvalidArgumentException;
use MiamiOH\ApplicationStatus\Label;
use MiamiOH\ApplicationStatus\Measurements;
use MiamiOH\ApplicationStatus\Status;
use MiamiOH\RESTng\Client\Agent;
use MiamiOH\RESTng\Client\Endpoint;
use MiamiOH\RESTng\Client\ResponseData;
use MiamiOH\RESTng\Client\RestNgClientException;
use MiamiOH\RESTng\Client\Token;
use MiamiOH\RESTng\Client\TokenProvider;
use Mockery;
use Tests\MiamiOH\ApplicationStatus\TestCase;
use Tests\MiamiOH\ApplicationStatus\TestStatusCheckImplementation;
use Tests\MiamiOH\ApplicationStatus\TestStatusCheckNotImplemented;

class ApplicationStatusTest extends TestCase
{
    private $appStatus;
    private $httpContainer = [];


    protected function setUp(): void
    {
        parent::setUp();
        $this->appStatus = new ApplicationStatus();
    }

    public function testCanGetDataPoints()
    {
        $this->appStatus->addHandler(function () {
            return new HttpConnectionDataPoint(
                'WebConnectionDataPoint::class',
                new Label(['Test']),
                new Measurements(),
                new Status('Success')
            );
        });
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertInstanceOf(DataPointCollection::class, $datapointCollection);
    }

    public function testDoesNotIncludeNonDataPoints()
    {
        $this->appStatus->addHandler(function () {
            return 'Foo';
        });
        $this->expectException(InvalidArgumentException::class);
        $this->appStatus->getDataPoints();
    }

    public function testCanCallHandlerMultipleTimes()
    {
        $this->appStatus->addHandler(function () {
            return new HttpConnectionDataPoint(
                'WebConnectionDataPoint::class',
                new Label(['Test']),
                new Measurements(),
                new Status('Success')
            );
        });
        $this->appStatus->addHandler(function () {
            return new HttpConnectionDataPoint(
                'WebConnectionDataPoint::class',
                new Label(['Test2']),
                new Measurements(),
                new Status('Failure')
            );
        });
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(2, $datapointCollection);
        $this->assertEquals('Test', $datapointCollection->first()->label());
    }

    /**
     * @dataProvider databaseHandlerDataProvider
     */
    public function testCanAddDatabaseHandler($label, $dbName, $status): void
    {
        if ($status === 'Success') {
            DB::shouldReceive('connection')
                ->once()
                ->with($dbName)
                ->andReturn(Mockery::mock('Illuminate\Database\Connection')
                );
        }

        $this->appStatus->addDatabaseHandler($label, $dbName);
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertEquals($label, $datapointCollection->first()->label());
        $this->assertEquals($status, $datapointCollection->first()->status());
    }

    public static function databaseHandlerDataProvider(): array
    {
        return [
            ['Test', 'oracle', 'Success'],
            ['Test2', 'fake_db', 'Failure']
        ];
    }

    public function testCanDiscoverAndAddHandlers(): void
    {
        $this->appStatus->discoverAndAddHandlers([TestStatusCheckImplementation::class]);
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertEquals('test label', $datapointCollection->first()->label());
        $this->assertEquals('Success', $datapointCollection->first()->status());
    }

    public function testWontAddHandlersForClassesThatDoNotImplementStatusCheckInterface()
    {
        $this->appStatus->discoverAndAddHandlers([TestStatusCheckNotImplemented::class]);
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(0, $datapointCollection);
    }

    public function testCanDiscoverAndAddHandlersWhenNoClassListProvided(): void
    {
        $testClass = new TestStatusCheckImplementation();
        $this->appStatus->discoverAndAddHandlers();
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertEquals('test label', $datapointCollection->first()->label());
        $this->assertEquals('Success', $datapointCollection->first()->status());
    }

    public function testCanAddRedisHandlerForDefaultConnection()
    {
        Redis::shouldReceive('connection')
            ->once()
            ->andReturn(Mockery::mock('Illuminate\Redis\Connection'));

        $this->appStatus->addRedisHandler(['Redis']);
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertEquals('Redis', $datapointCollection->first()->label());
        $this->assertEquals('Success', $datapointCollection->first()->status());
    }

    /**
     * @throws InvalidArgumentException
     */
    public function testCanAddRedisHandlerForSpecificConnection()
    {
        Redis::shouldReceive('connection')
            ->once()
            ->with('TestRedis')
            ->andReturn(Mockery::mock('Illuminate\Redis\Connection'));

        $this->appStatus->addRedisHandler(['Redis'], 'TestRedis');
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertEquals('Redis', $datapointCollection->first()->label());
        $this->assertEquals('Success', $datapointCollection->first()->status());
    }

    /**
     * @throws InvalidArgumentException
     */
    public function testCanAddRedisHandlerAndFailDefaultConnection()
    {
        Redis::shouldReceive('connection')
            ->once()
            ->andThrowExceptions([new \Exception]);

        $this->appStatus->addRedisHandler(['Redis']);
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertEquals('Redis', $datapointCollection->first()->label());
        $this->assertEquals('Failure', $datapointCollection->first()->status());
    }

    public function testCanCheckRestNGForAuthenticationAndFail()
    {
        $endpoint = $this->createMock(Endpoint::class);
        $tokenProvider = $this->createMock(TokenProvider::class);
        $tokenProvider->expects($this->once())
            ->method('getToken')
            ->willThrowException(new RestNgClientException());

        $this->appStatus->addRestNgAuthenticationCheck($endpoint,$tokenProvider);
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertInstanceOf(RestNgDataPoint::class, $datapointCollection->first());
        $this->assertEquals('Failure', $datapointCollection->first()->status());
    }

    public function testCanCheckRestNGForAuthenticationSuccessfully()
    {
        $endpoint = $this->createMock(Endpoint::class);
        $tokenProvider = $this->createMock(TokenProvider::class);
        $tokenProvider->expects($this->once())
            ->method('getToken');

        $this->appStatus->addRestNgAuthenticationCheck($endpoint,$tokenProvider);
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertInstanceOf(RestNgDataPoint::class, $datapointCollection->first());
        $this->assertEquals('Success', $datapointCollection->first()->status());
    }

    public function testGetDataPointsCanHandleErrors()
    {
        $this->appStatus->addHandler(function () {
            trigger_error('Some Error Created');
        });
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertInstanceOf(DataPointCollection::class, $datapointCollection);
    }
    public function testGetDataPointsCanHandleException()
    {
        $this->appStatus->addHandler(function () {
            throw new \Exception('Some Exception Created');
        });
        $datapointCollection = $this->appStatus->getDataPoints();
        $this->assertCount(1, $datapointCollection);
        $this->assertInstanceOf(DataPointCollection::class, $datapointCollection);
        $this->assertEquals('Failure', $datapointCollection->first()->status());    }

        public function testCanCheckConfigurationServiceForAuthenticationSuccessfully()
        {
            $agentMock = $this->getMockBuilder(Agent::class)
                ->setConstructorArgs([$this->createMock(Endpoint::class)])
                ->enableOriginalConstructor()
                ->getMock();
    
            $responseMock = $this->getMockBuilder(ResponseData::class)
                ->disableOriginalConstructor()
                ->getMock();
            $responseMock->method('status')
                ->willReturn(200);
    
            $agentMock->expects($this->once())
                ->method('run')
                ->with($this->isInstanceOf(Request::class))
                ->willReturn($responseMock);
    
            $this->appStatus->addConfigurationServiceCheck($agentMock, 'application', 'category');
            $datapointCollection = $this->appStatus->getDataPoints();
    
            $this->assertInstanceOf(RestNgDataPoint::class, $datapointCollection->first());
            $this->assertEquals('Success', $datapointCollection->first()->status());
        }
    
        public function testCanCheckConfigurationServiceForAuthenticationAndFail()
        {
            $agentMock = $this->getMockBuilder(Agent::class)
                ->setConstructorArgs([$this->createMock(Endpoint::class)])
                ->enableOriginalConstructor()
                ->getMock();
    
            $responseMock = $this->getMockBuilder(ResponseData::class)
                ->disableOriginalConstructor()
                ->getMock();
            $responseMock->method('status')
                ->willReturn(401);
    
            $agentMock->expects($this->once())
                ->method('run')
                ->with($this->isInstanceOf(Request::class))
                ->willReturn($responseMock);
    
            $this->appStatus->addConfigurationServiceCheck($agentMock, 'application', 'category');
            $datapointCollection = $this->appStatus->getDataPoints();
    
            $this->assertInstanceOf(RestNgDataPoint::class, $datapointCollection->first());
            $this->assertEquals('Failure', $datapointCollection->first()->status());
        }
}
