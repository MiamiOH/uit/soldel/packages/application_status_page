<?php


namespace Tests\MiamiOH\ApplicationStatus\Unit;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use Tests\MiamiOH\ApplicationStatus\TestCase;

class ClientTestCase extends TestCase
{
    /**
     * @var array
     */
    public $container;


    protected function newHttpClientWithResponses(array $responses): Client
    {
        $mock = new MockHandler($responses);

        $this->container = [];
        $history = Middleware::history($this->container);

        $handler = HandlerStack::create($mock);
        $handler->push($history);

        return new Client(['handler' => $handler]);
    }

    protected function newJsonResponse(array $content, int $status = 200, array $headers = []): Response
    {
        $body = json_encode($content);

        if (empty($headers)) {
            $headers['content-type'] = 'application/json';
            $headers['content-length'] = strlen($body);
        }

        return new Response(
            $status,
            $headers,
            $body
        );
    }
}
