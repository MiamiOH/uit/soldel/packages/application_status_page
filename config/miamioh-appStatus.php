<?php

return [
    'baseThemeLayout' => env('STATUS_THEME', 'ApplicationStatus::simpleLayout'),
    'themeSection' => env('STATUS_SECTION', 'content')
];
