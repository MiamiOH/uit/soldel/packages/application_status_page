@extends(config('miamioh-appStatus.baseThemeLayout'))

@push(config('miamioh-appStatus.themeSection'))

    <div class="container-fluid">
        <table class="table table-condensed table-bordered table-miami data-table">
            <thead>
            <tr>
                <th>Entity</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dataPointCollection as $dataPoint)
                <tr>
                    <td>
                        {!! $dataPoint->getData()['label'] !!}
                    </td>
                    <td>
                        {!! $dataPoint->getData()['status'] !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


@endpush


