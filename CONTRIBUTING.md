How to contribute to this package
===================================

## Create a Topic Branch
* Clone or Fork the Repo
* Push your changes to a topic branch in your fork of the repository.
* Do all of your work for this feature or fix on this topic branch
* Make commits with clear messages and include the Issue or Ticket 
* Update the ticket or issue, as you work, and communicate with others


## Test You Changes

* All code should be covered with a test.
* Code was written with both unit and feature tests.   
* The tests utilize [`orchestra/testbench`](https://github.com/orchestral/testbench) which provides the ability to write tests for Laravel Packages.

